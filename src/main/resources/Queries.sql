CREATE SCHEMA if not exists schema1;
CREATE SCHEMA if not exists schema2;
CREATE SCHEMA if not exists schema3;

CREATE TABLE if not exists schema1.users
(
    Id       int4 PRIMARY KEY,
    nickname varchar,
    country  varchar,
    city     varchar,
    login    varchar,
    password varchar
);

CREATE TABLE if not exists schema2.users
(
    Id       int4 PRIMARY KEY,
    nickname varchar,
    country  varchar,
    city     varchar,
    login    varchar,
    password varchar
);

CREATE TABLE if not exists schema3.users
(
    Id       int4 PRIMARY KEY,
    nickname varchar,
    country  varchar,
    city     varchar,
    login    varchar,
    password varchar
);

DELETE FROM schema1.users;
DELETE FROM schema2.users;
DELETE FROM schema3.users;

insert into schema1.users values (3, 'nickname3', 'country3', 'city3', 'login3', 'password3');
insert into schema1.users values (6, 'nickname6', 'country6', 'city6', 'login6', 'password6');
insert into schema1.users values (9, 'nickname9', 'country9', 'city9', 'login9', 'password9');

insert into schema2.users values (1, 'nickname1', 'country1', 'city1', 'login1', 'password1');
insert into schema2.users values (4, 'nickname4', 'country4', 'city4', 'login4', 'password4');
insert into schema2.users values (7, 'nickname7', 'country7', 'city7', 'login7', 'password7');

insert into schema3.users values (2, 'nickname2', 'country2', 'city2', 'login2', 'password2');
insert into schema3.users values (5, 'nickname5', 'country5', 'city5', 'login5', 'password5');
insert into schema3.users values (8, 'nickname8', 'country8', 'city8', 'login8', 'password8');