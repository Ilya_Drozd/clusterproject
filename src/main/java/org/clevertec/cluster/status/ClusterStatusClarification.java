package org.clevertec.cluster.status;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.configs.PortsConfig;
import org.clevertec.cluster.constants.Messages;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Configuration
@EnableScheduling
@Slf4j
@RequiredArgsConstructor
public class ClusterStatusClarification {

    private final PortsConfig portsConf;
    private final ServerProperties serverProperties;
    private final RestTemplate restTemplate;

    @Getter
    @Setter
    private static HashMap<Integer, Boolean> nodes = new HashMap<>();

    @Scheduled(fixedRate = 100000, initialDelay = 20000)
    public void checkClusterStatus() {
        List<Integer> ports = findPorts();
        for (Integer port : ports) {
            try {
                restTemplate.getForEntity("http://localhost:" + port + "/status", String.class);
                nodes.put(port, true);
                log.info(Messages.NODE_AVAILABLE, port);
            } catch (ResourceAccessException e) {
                nodes.put(port, false);
                log.warn(Messages.NODE_UNAVAILABLE, port);
            }
        }
    }

    private List<Integer> findPorts() {
        List<Integer> ports = new ArrayList<>();
        ports.add(portsConf.getPort1());
        ports.add(portsConf.getPort2());
        ports.add(portsConf.getPort3());
        ports.remove(serverProperties.getPort());
        return ports;
    }

    public boolean isNodeAvailable(int port) {
        for (HashMap.Entry<Integer, Boolean> node : nodes.entrySet()) {
            if (node.getKey() == port) {
                return node.getValue();
            }
        }
        return false;
    }
}
