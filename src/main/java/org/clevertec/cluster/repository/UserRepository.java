package org.clevertec.cluster.repository;

import org.clevertec.cluster.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
