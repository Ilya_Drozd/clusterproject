package org.clevertec.cluster.configs;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "ports")
@Getter
@Setter
public class PortsConfig {

    private int port1;

    private int port2;

    private int port3;

}
