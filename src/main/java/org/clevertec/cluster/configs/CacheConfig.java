package org.clevertec.cluster.configs;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "cache")
@Getter
@Setter
public class CacheConfig {

    private String cacheType;

    private int cacheCapacity;

}
