package org.clevertec.cluster.sql;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.configs.SpringJdbcConfig;
import org.clevertec.cluster.configuration.ReadFileToString;
import org.clevertec.cluster.constants.FileNames;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;

@Component
@Slf4j
@RequiredArgsConstructor
public class SqlExecutor {

    private final SpringJdbcConfig config;
    @Setter
    private JdbcTemplate jdbcTemplate;
    private ReadFileToString reader = new ReadFileToString();
    private List<String> queries = new ArrayList<>();

    @PostConstruct
    private void init() {
        jdbcTemplate = new JdbcTemplate(config.getDataSource());
    }

    public void executeQuery(String schema, LinkedHashMap<Integer, String> schemas) {
        parseLine(reader.readFile(FileNames.SQL_FILE_NAME));
        List<String> schem = new ArrayList<>(schemas.values());
        List<String> queries;
        for (String sch : schem) {
            if (sch.equals(schema)) {
                queries = findQueriesForSchema(sch);
                for (String query : queries) {
                    jdbcTemplate.execute(query);
                }
            }
        }
    }

    private void parseLine(String line) {
        Pattern pattern = Pattern.compile(";");
        String[] lines = pattern.split(line, 0);
        for (String l : lines) {
            try {
                queries.add(l);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
    }

    private List<String> findQueriesForSchema(String schema) {
        List<String> queriesForSchema = new ArrayList<>();
        for (String query : queries) {
            if (query.lastIndexOf(schema) != -1) {
                queriesForSchema.add(query);
            }
        }
        return queriesForSchema;
    }
}
