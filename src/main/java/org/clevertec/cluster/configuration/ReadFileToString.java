package org.clevertec.cluster.configuration;

import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.constants.Messages;

import java.io.*;

@Slf4j
public class ReadFileToString {

    public String readFile(String fileName) {
        File file = new File(getClass().getClassLoader().getResource(fileName).getFile());
        try(FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr)) {

            StringBuilder builder = new StringBuilder();
            String line = reader.readLine();
            while (line != null) {
                builder.append(line);
                line = reader.readLine();
            }
            return String.valueOf(builder);
        } catch (Exception e) {
            log.error(Messages.UNABLE_TO_READ_FILE);
        }
        return null;
    }
}
