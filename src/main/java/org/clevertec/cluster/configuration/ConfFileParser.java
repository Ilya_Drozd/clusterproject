package org.clevertec.cluster.configuration;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.constants.Messages;

import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Getter
class ConfFileParser {

    private LinkedHashMap<Integer, Integer> ports = new LinkedHashMap<>();
    private LinkedHashMap<Integer, String> schemas = new LinkedHashMap<>();

    void parseLine(String line) {
        Pattern pattern = Pattern.compile(";");
        String[] lines = pattern.split(line, 0);
        for (String l : lines) {
            try {
                ports.put(findNodeNumber(l), findPort(l));
                schemas.put(findNodeNumber(l), findSchema(l));
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
    }

    private int findPort(String line) throws Exception {
        Pattern pattern = Pattern.compile("\\s\\d+");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            return Integer.parseInt(matcher.group().replace(" ", ""));
        } else {
            throw new Exception(Messages.PORT_NOT_FOUND);
        }
    }

    private String findSchema(String line) throws Exception {
        Pattern pattern = Pattern.compile("[:]\\s[a-z]+[0-9]+");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            return matcher.group().replace(": ", "");
        } else {
            throw new Exception(Messages.SCHEMA_NOT_FOUND);
        }
    }

    private Integer findNodeNumber(String line) throws Exception {
        Pattern pattern = Pattern.compile("node[0-9]+");
        Matcher matcher = pattern.matcher(line);
        if(matcher.find()){
            return Integer.valueOf(matcher.group().replace("node", ""));
        } else {
            throw new Exception(Messages.NODE_NUMBER_NOT_FOUND);
        }
    }
}
