package org.clevertec.cluster.configuration;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.configs.ClusterConfig;
import org.clevertec.cluster.configs.PortsConfig;
import org.clevertec.cluster.constants.FileNames;
import org.clevertec.cluster.sql.SqlExecutor;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;

@Component
@Slf4j
@RequiredArgsConstructor
public class NodeConfiguration implements WebServerFactoryCustomizer<ConfigurableWebServerFactory> {

    private final DataSourceProperties dataSourceProperties;
    private final ClusterConfig clusterConfig;
    private final PortsConfig portsConfig;
    private final ServerProperties serverProperties;
    private final SqlExecutor executor;

    private ReadFileToString fileReader = new ReadFileToString();
    private ConfFileParser parser = new ConfFileParser();

    private LinkedHashMap<Integer, Integer> ports = new LinkedHashMap<>();
    private LinkedHashMap<Integer, String> schemas = new LinkedHashMap<>();

    private int port;
    private String schema;

    @Override
    public void customize(ConfigurableWebServerFactory factory) {
        configureNode();
        executor.executeQuery(schema, schemas);
        dataSourceProperties.setUrl(createUrl(schema));
        factory.setPort(port);
        serverProperties.setPort(port);
        setPorts(ports);
    }

    private void configureNode() {
        parser.parseLine(fileReader.readFile(FileNames.CONF_FILE_NAME));
        ports = parser.getPorts();
        schemas = parser.getSchemas();
        int nodeNumber = clusterConfig.getNodeNumber();
        port = ports.get(nodeNumber);
        schema = schemas.get(nodeNumber);
    }

    private String createUrl(String schema) {
        String url = dataSourceProperties.getUrl();
        return url + "?currentSchema=" + schema;
    }

    private void setPorts(LinkedHashMap<Integer, Integer> ports) {
        portsConfig.setPort1(ports.get(1));
        portsConfig.setPort2(ports.get(2));
        portsConfig.setPort3(ports.get(3));
    }
}
