package org.clevertec.cluster.constants;

public final class Cases {

    //org.clevertec.cluster.cache.factory.CacheFactory
    public static final String LFU_CACHE = "lfu";
    public static final String LRU_CACHE = "lru";

    //org.clevertec.cluster.filter.RequestSend
    public static final String POST_REQUEST = "POST";
    public static final String PUT_REQUEST = "PUT";
    public static final String DELETE_REQUEST = "DELETE";

}
