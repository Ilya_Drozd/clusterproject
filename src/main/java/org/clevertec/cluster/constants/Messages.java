package org.clevertec.cluster.constants;

public final class Messages {

    //org.clevertec.cluster.cache.factory.CacheFactory
    public static final String LFU_CACHE_TYPE = "LFU cache type";
    public static final String LRU_CACHE_TYPE = "LRU cache type";
    public static final String UNKNOWN_CACHE_TYPE = " is unknown cache type";

    //org.clevertec.cluster.cache.operations.impl.CacheOperationsImpl
    public static final String ADDING_USER_IN_CACHE = "Adding user {} in cache";
    public static final String UPDATING_USER_IN_CACHE = "Updating user {} in cache";
    public static final String GETTING_USER_WITH_ID_FROM_CACHE = "User with id = {} received successful from the cache";
    public static final String REMOVING_USER_WITH_ID_FROM_CACHE = "Removing user with id = {} from the cache";
    public static final String REMOVING_USER_WITH_ID_FROM_CACHE_FAILED = "User with id = {} has not been removed from the cache";

    //org.clevertec.cluster.service.impl.UserServiceImpl
    public static final String ADDING_USER_IN_DATABASE = "User {} added successful";
    public static final String UPDATING_USER_IN_DATABASE = "User {} update successful in the database";
    public static final String GETTING_USER_WITH_ID_FROM_DATABASE = "User with id = {} received successful from the database";
    public static final String USER_NOT_FOUND = "User with id = {} was not found";
    public static final String GETTING_USERS_FROM_DATABASE = "User pageable received : {}";
    public static final String REMOVING_USER_WITH_ID_FROM_DATABASE = "User with id = {} removed successful from the database";
    public static final String REPRESENTING_USER_TO_JSON_FAILED = "can't represent object of class {} in json form for logging: {}";

    //org.clevertec.cluster.filter.RequestSend
    public static final String UNKNOWN_REQUEST_TYPE = "Unknown request type";

    //org.clevertec.cluster.configuration.ConfFileParser
    public static final String PORT_NOT_FOUND = "Could not find port. Incorrect data";
    public static final String SCHEMA_NOT_FOUND = "Could not find schema. Incorrect data";
    public static final String NODE_NUMBER_NOT_FOUND = "Could not find node number. Incorrect data";

    //org.clevertec.cluster.configuration.ReadFileToString
    public static final String UNABLE_TO_READ_FILE = "Unable to read file";

    //org.clevertec.cluster.filter.AppFilter
    public static final String REQUEST_REDIRECT = "Redirect {} request to {}";
    public static final String UNABLE_TO_CONNECT = "Unable to connect. ";
    public static final String UNKNOWN_PORT = "Unknown port";
    public static final String INCORRECT_URL = "Incorrect url";

    //org.clevertec.cluster.status.ClusterStatusClarification
    public static final String NODE_AVAILABLE = "Node with port {} is available";
    public static final String NODE_UNAVAILABLE = "Node with port {} is unavailable";
}
