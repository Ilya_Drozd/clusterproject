package org.clevertec.cluster.constants;

public final class FileNames {

    public final static String SQL_FILE_NAME = "Queries.sql";
    public final static String  CONF_FILE_NAME = "ClusterConfiguration.txt";

}
