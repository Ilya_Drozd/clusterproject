package org.clevertec.cluster.filter;

import lombok.RequiredArgsConstructor;
import org.clevertec.cluster.constants.Cases;
import org.clevertec.cluster.constants.Messages;
import org.clevertec.cluster.dto.User;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
class RequestSend {

    private final RestTemplate restTemplate;

    void sendRequest(String method, User user, String url) throws Exception {
        switch (method) {
            case Cases.POST_REQUEST:
                sendPost(url, user);
                break;
            case Cases.PUT_REQUEST:
                sendPut(url, user);
                break;
            case Cases.DELETE_REQUEST:
                sendDelete(url);
                break;
            default:
                throw new Exception(Messages.UNKNOWN_REQUEST_TYPE);
        }
    }

    private void sendPost(String url, User user) {
        HttpEntity<User> requestBody = new HttpEntity<>(user);
        restTemplate.postForLocation(url, requestBody, User.class);
    }

    private void sendPut(String url, User user) {
        HttpEntity<User> requestBody = new HttpEntity<>(user);
        restTemplate.put(url, requestBody, User.class);
    }

    private void sendDelete(String url) {
        restTemplate.delete(url);
    }
}
