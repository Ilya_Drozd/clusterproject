package org.clevertec.cluster.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.constants.Messages;
import org.clevertec.cluster.dto.User;
import org.clevertec.cluster.configs.PortsConfig;
import org.clevertec.cluster.servlet.CustomHttpServletRequestWrapper;
import org.clevertec.cluster.status.ClusterStatusClarification;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
@RequiredArgsConstructor
public class AppFilter implements Filter {

    private final PortsConfig portsConf;
    private final ServerProperties serverProperties;
    private final ObjectMapper objectMapper;
    private final ClusterStatusClarification clusterStatusClarification;
    private final RequestSend requestSend;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        CustomHttpServletRequestWrapper requestWrapper = new CustomHttpServletRequestWrapper(req);
        String jsonUser = getUserToJson(requestWrapper);
        String url = String.valueOf(req.getRequestURL());
        try {
            if (isGetAllRequest(url) || isClusterStatusRequest(url)) {
                chain.doFilter(requestWrapper, response);
            } else {
                int port = getPort(getId(jsonUser, url, req));
                if (port == serverProperties.getPort()) {
                    chain.doFilter(requestWrapper, response);
                } else {
                    URI uri = new URI(url);
                    String newUrl = uri.getScheme() + "://" + uri.getHost() + ":" + port + uri.getRawPath();
                    if (clusterStatusClarification.isNodeAvailable(port)) {
                        if (((HttpServletRequest) request).getMethod().equals(HttpMethod.GET.name())) {
                            log.info(Messages.REQUEST_REDIRECT, ((HttpServletRequest) request).getMethod(), newUrl);
                            ((HttpServletResponse) response).sendRedirect(newUrl);
                        } else {
                            log.info(Messages.REQUEST_REDIRECT, ((HttpServletRequest) request).getMethod(), newUrl);
                            requestSend.sendRequest(req.getMethod(), getUserFromJson(jsonUser), newUrl);
                        }
                    } else {
                        log.warn(Messages.UNABLE_TO_CONNECT + Messages.NODE_UNAVAILABLE, port);
                        ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private int getPort(int id) throws Exception {
        int result = id % 3;
        if (result == 0) {
            return portsConf.getPort1();
        } else if (result == 1) {
            return portsConf.getPort2();
        } else if (result == 2) {
            return portsConf.getPort3();
        } else {
            throw new Exception(Messages.UNKNOWN_PORT);
        }
    }

    private String getUserToJson(CustomHttpServletRequestWrapper requestWrapper) throws IOException {
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = requestWrapper.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        return String.valueOf(buffer);
    }

    private User getUserFromJson(String jsonUser) throws IOException {
        if (!jsonUser.equals("")) {
            return objectMapper.readValue(jsonUser, User.class);
        } else {
            return new User();
        }
    }

    private boolean isGetAllRequest(String url) {
        Pattern pattern = Pattern.compile("/all");
        Matcher matcher = pattern.matcher(url);
        return matcher.find();
    }

    private boolean isClusterStatusRequest(String url) {
        Pattern pattern = Pattern.compile("/status");
        Matcher matcher = pattern.matcher(url);
        return matcher.find();
    }

    private int getId(String jsonUser, String url, HttpServletRequest req) throws Exception {
        if (req.getMethod().equals(HttpMethod.GET.name()) || req.getMethod().equals(HttpMethod.DELETE.name())) {
            Pattern pattern = Pattern.compile("/[0-9]+$");
            Matcher matcher = pattern.matcher(url);
            if (matcher.find()) {
                return Integer.parseInt(matcher.group().replace("/", ""));
            } else {
                throw new Exception(Messages.INCORRECT_URL);
            }
        } else {
            User user = objectMapper.readValue(jsonUser, User.class);
            return user.getId();
        }
    }
}
