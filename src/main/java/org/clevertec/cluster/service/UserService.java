package org.clevertec.cluster.service;

import org.clevertec.cluster.dto.User;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface UserService {
    Integer save(User user);
    Optional<User> find(Integer id);
    Page<User> findAll(Integer pageSize, Integer pageNumber);
    Integer update(User user);
    Integer delete(Integer id);
}
