package org.clevertec.cluster.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.cache.operations.CacheOperations;
import org.clevertec.cluster.constants.Messages;
import org.clevertec.cluster.dto.User;
import org.clevertec.cluster.exceptions.NoSuchUserException;
import org.clevertec.cluster.repository.UserRepository;
import org.clevertec.cluster.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final ObjectMapper objectMapper;
    private final CacheOperations cacheOperations;

    @Override
    public Integer save(User user) {
        repository.save(user);
        log.info(Messages.ADDING_USER_IN_DATABASE, toJson(user));
        return user.getId();
    }

    @Override
    public Optional<User> find(Integer id) {
        Optional<User> user = Optional.ofNullable(cacheOperations.getById(id));
        if (user.equals(Optional.empty())) {
            user = repository.findById(id);
            if (user.isPresent()) {
                cacheOperations.add(user.get());
                log.info(Messages.GETTING_USER_WITH_ID_FROM_DATABASE, id);
            } else {
                log.warn(Messages.USER_NOT_FOUND, id);
                throw new NoSuchUserException();
            }
        }
        return user;
    }

    private Optional<User> findWithoutAddingInCache(Integer id) {
        Optional<User> user = repository.findById(id);
        if (!user.isPresent()) {
            log.warn(Messages.USER_NOT_FOUND, id);
            throw new NoSuchUserException();
        }
        return user;
    }

    @Override
    public Page<User> findAll(Integer pageSize, Integer pageNumber) {
        Page<User> users = repository.findAll(PageRequest.of(pageNumber, pageSize));
        log.info(Messages.GETTING_USERS_FROM_DATABASE, toJson(users));
        return users;
    }

    @Override
    public Integer update(User user) {
        if(findWithoutAddingInCache(user.getId()).isPresent()){
            repository.save(user);
            log.info(Messages.UPDATING_USER_IN_DATABASE, toJson(user));
            cacheOperations.update(user);
        }
        return user.getId();
    }

    @Override
    public Integer delete(Integer id) {
        if(findWithoutAddingInCache(id).isPresent()) {
            repository.deleteById(id);
            log.info(Messages.REMOVING_USER_WITH_ID_FROM_DATABASE, id);
            cacheOperations.delete(id);
        }
        return id;
    }

    private String toJson(Object o) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.warn(Messages.REPRESENTING_USER_TO_JSON_FAILED, o.getClass().getSimpleName(), e.toString());
        }
        return json;
    }
}
