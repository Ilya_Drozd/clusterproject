package org.clevertec.cluster.cache.logic.impl;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.clevertec.cluster.cache.logic.CacheLogic;
import org.clevertec.cluster.dto.User;

import java.util.LinkedList;
import java.util.List;

@Getter
@RequiredArgsConstructor
public class LRUCacheLogic implements CacheLogic {

    @NonNull
    private int cacheCapacity;
    private List<User> cache = new LinkedList<>();

    @Override
    public boolean isExistInCache(int id) {
        for(User user : cache){
            if(user.getId() == id){
                return true;
            }
        }
        return false;
    }

    @Override
    public void add(User user) {
        if(cache.size() < cacheCapacity){
            cache.add(0, user);
        } else {
            deleteNotActualElement();
            cache.add(0, user);
        }
    }

    @Override
    public void update(User user) {
        User desiredUser = null;
        for (User value : cache) {
            if (user.getId().equals(value.getId())) {
                desiredUser = value;
                break;
            }
        }
        cache.remove(desiredUser);
        add(user);
    }

    @Override
    public User getById(int id) {
        User desiredUser = null;
        for(User user : cache){
            if(user.getId().equals(id)){
                desiredUser = user;
                break;
            }
        }
        cache.remove(desiredUser);
        add(desiredUser);
        return desiredUser;
    }

    @Override
    public void deleteById(int id) {
        User desiredUser = new User();
        for(User user : cache){
            if(user.getId().equals(id)){
                desiredUser = user;
                break;
            }
        }
        cache.remove(desiredUser);
    }

    @Override
    public void deleteNotActualElement() {
        cache.remove(cacheCapacity - 1);
    }
}
