package org.clevertec.cluster.cache.logic;

import org.clevertec.cluster.dto.User;


public interface CacheLogic {
    boolean isExistInCache(int id);
    void add(User user);
    void update(User user);
    User getById(int id);
    void deleteById(int id);
    void deleteNotActualElement();
}
