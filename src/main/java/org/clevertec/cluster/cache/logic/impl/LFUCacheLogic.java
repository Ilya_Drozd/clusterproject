package org.clevertec.cluster.cache.logic.impl;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.clevertec.cluster.cache.logic.CacheLogic;
import org.clevertec.cluster.dto.User;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Getter
@RequiredArgsConstructor
public class LFUCacheLogic implements CacheLogic {

    @NonNull
    private int cacheCapacity;
    private LinkedHashMap<User, Integer> cache = new LinkedHashMap<>(cacheCapacity);

    @Override
    public boolean isExistInCache(int id) {
        Set<User> users = cache.keySet();
        for (User user : users) {
            if (user.getId() == id) {
                return cache.containsKey(user);
            }
        }
        return false;
    }

    @Override
    public void add(User user) {
        if (cache.size() < cacheCapacity) {
            cache.put(user, 1);
        } else {
            deleteNotActualElement();
            cache.put(user, 1);
        }
    }

    @Override
    public void update(User user) {
        int id = user.getId();
        int value = 0;
        User desiredUser = new User();
        Set<User> users = cache.keySet();
        for (User user1 : users) {
            if (user1.getId() == id) {
                value = cache.get(user1);
                desiredUser = user1;
            }
        }
        cache.remove(desiredUser);
        if(cache.size() == cacheCapacity){
            deleteNotActualElement();
        }
        cache.put(user, ++value);
    }

    @Override
    public User getById(int id) {
        Set<User> users = cache.keySet();
        for (User user : users) {
            if (user.getId() == id) {
                int value = cache.get(user);
                cache.put(user, ++value);
                return user;
            }
        }
        return null;
    }

    @Override
    public void deleteById(int id) {
        User desiredUser = new User();
        Set<User> users = cache.keySet();
        for (User user : users) {
            if (user.getId() == id) {
                desiredUser = user;
            }
        }
        cache.remove(desiredUser);
    }

    @Override
    public void deleteNotActualElement() {
        int minValue;
        User user = null;

        Iterator<Map.Entry<User, Integer>> iterator = cache.entrySet().iterator();
        Map.Entry<User, Integer> entry = iterator.next();
        minValue = entry.getValue();

        for (Map.Entry<User, Integer> item : cache.entrySet()) {
            if (item.getValue() <= minValue) {
                minValue = item.getValue();
                user = item.getKey();
            }
        }
        cache.remove(user);
    }
}
