package org.clevertec.cluster.cache.factory;

import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.cache.logic.CacheLogic;
import org.clevertec.cluster.cache.logic.impl.LFUCacheLogic;
import org.clevertec.cluster.cache.logic.impl.LRUCacheLogic;
import org.clevertec.cluster.constants.Cases;
import org.clevertec.cluster.constants.Messages;

@Slf4j
public class CacheFactory {

    public static CacheLogic createCache(String type, int capacity) {
        switch (type.toLowerCase()) {
            case Cases.LFU_CACHE:
                log.info(Messages.LFU_CACHE_TYPE);
                return new LFUCacheLogic(capacity);
            case Cases.LRU_CACHE:
                log.info(Messages.LRU_CACHE_TYPE);
                return new LRUCacheLogic(capacity);
            default:
                throw new RuntimeException(type + Messages.UNKNOWN_CACHE_TYPE);
        }
    }

}
