package org.clevertec.cluster.cache.operations;

import org.clevertec.cluster.dto.User;

public interface CacheOperations {
    void add(User user);
    User getById(int id);
    void update(User user);
    void delete(int id);
}
