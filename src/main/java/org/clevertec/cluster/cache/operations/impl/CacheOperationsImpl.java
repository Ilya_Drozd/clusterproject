package org.clevertec.cluster.cache.operations.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.cluster.cache.operations.CacheOperations;
import org.clevertec.cluster.configs.CacheConfig;
import org.clevertec.cluster.cache.factory.CacheFactory;
import org.clevertec.cluster.cache.logic.CacheLogic;
import org.clevertec.cluster.constants.Messages;
import org.clevertec.cluster.dto.User;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
@Slf4j
@RequiredArgsConstructor
public class CacheOperationsImpl implements CacheOperations {

    private final CacheConfig cacheConfig;
    private CacheLogic cacheLogic;

    @PostConstruct
    private void init(){
        cacheLogic =  CacheFactory.createCache(cacheConfig.getCacheType(), cacheConfig.getCacheCapacity());
    }

    @Override
    public void add(User user) {
        cacheLogic.add(user);
        log.info(Messages.ADDING_USER_IN_CACHE, user);
    }

    @Override
    public void update(User user) {
        if(cacheLogic.isExistInCache(user.getId())){
            cacheLogic.update(user);
            log.info(Messages.UPDATING_USER_IN_CACHE, user);
        } else {
            cacheLogic.update(user);
            log.info(Messages.ADDING_USER_IN_CACHE, user);
        }
    }

    @Override
    public User getById(int id) {
        if(cacheLogic.isExistInCache(id)){
            User user = cacheLogic.getById(id);
            log.info(Messages.GETTING_USER_WITH_ID_FROM_CACHE, id);
            return user;
        } else {
            return null;
        }
    }

    @Override
    public void delete(int id) {
        if(cacheLogic.isExistInCache(id)){
            cacheLogic.deleteById(id);
            if(!cacheLogic.isExistInCache(id)){
                log.info(Messages.REMOVING_USER_WITH_ID_FROM_CACHE, id);
            } else {
                log.info(Messages.REMOVING_USER_WITH_ID_FROM_CACHE_FAILED, id);
            }
        }
    }
}
