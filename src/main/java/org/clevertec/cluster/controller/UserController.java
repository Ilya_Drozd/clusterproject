package org.clevertec.cluster.controller;

import lombok.RequiredArgsConstructor;
import org.clevertec.cluster.dto.User;
import org.clevertec.cluster.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    @PostMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> save(@RequestBody User user) {
        return ResponseEntity.ok(service.save(user));
    }

    @GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable Integer id) {
        return ResponseEntity.ok(service.find(id));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll(
            @RequestParam(name = "pagesize", defaultValue = "15", required = false)
                    Integer pageSize,
            @RequestParam(name = "pagenumber", defaultValue = "0", required = false)
                    Integer pageNumber) {

        return ResponseEntity.ok(service.findAll(pageSize, pageNumber));
    }

    @PutMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> update(@RequestBody User user) {
        return ResponseEntity.ok(service.update(user));
    }

    @DeleteMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        return ResponseEntity.ok(service.delete(id));
    }
}
