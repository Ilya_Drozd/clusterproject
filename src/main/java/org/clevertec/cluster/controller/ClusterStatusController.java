package org.clevertec.cluster.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/status")
public class ClusterStatusController {

    @GetMapping
    public ResponseEntity<?> getStatus(){
        return ResponseEntity.ok("The node is connected");
    }
}
