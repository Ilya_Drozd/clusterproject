package org.clevertec.cluster.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"cluster.node-number = 1",  "server.port = 9000", "ports.port1 = 9000",
        "ports.port2 = 9001", "ports.port3 = 9002"})
@SpringBootTest
public class ClusterStatusClarificationTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private ClusterStatusClarification clusterStatusClarification;

    private HashMap<Integer, Boolean> nodes = new HashMap<>();


    @Test
    public void checkClusterStatusWhenAllNodesAreAvailable() {
        nodes.put(9001, true);
        nodes.put(9002, true);

        given(this.restTemplate.getForEntity(anyString(), any())).willReturn(ResponseEntity.ok(""));

        clusterStatusClarification.checkClusterStatus();
        assertEquals(nodes, ClusterStatusClarification.getNodes());
    }

    @Test
    public void checkClusterStatusWhenAllNodesAreNotAvailable() {
        nodes.put(9001, false);
        nodes.put(9002, false);

        given(this.restTemplate.getForEntity(anyString(), any())).willThrow(ResourceAccessException.class);

        clusterStatusClarification.checkClusterStatus();
        assertEquals(nodes, ClusterStatusClarification.getNodes());
    }

    @Test
    public void getNodeStatus() {
        nodes.put(9001, true);
        nodes.put(9002, false);

        ClusterStatusClarification.setNodes(nodes);
        assertTrue(clusterStatusClarification.isNodeAvailable(9001));
        assertFalse(clusterStatusClarification.isNodeAvailable(9002));
    }
}