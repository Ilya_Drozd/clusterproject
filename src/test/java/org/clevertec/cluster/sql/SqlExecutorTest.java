package org.clevertec.cluster.sql;

import org.clevertec.cluster.configs.SpringJdbcConfig;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.LinkedHashMap;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;

public class SqlExecutorTest {

    private SqlExecutor sqlExecutor = new SqlExecutor(new SpringJdbcConfig(new DataSourceProperties()));
    private final JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);
    private LinkedHashMap<Integer, String> schemas = new LinkedHashMap<>();

    private final static String SQL_QUERY = "CREATE SCHEMA if not exists schema1";

    @Before
    public void jdbcTemplateInit(){
        sqlExecutor.setJdbcTemplate(jdbcTemplate);
    }

    @Test
    public void executeQuery() {
        schemas.put(1, "schema1");
        schemas.put(2, "schema2");
        schemas.put(3, "schema3");

        doNothing().when(jdbcTemplate).execute(anyString());

        sqlExecutor.executeQuery("schema1", schemas);
        Mockito.verify(jdbcTemplate).execute(SQL_QUERY);
    }
}