package org.clevertec.cluster.service;

import org.clevertec.cluster.cache.operations.CacheOperations;
import org.clevertec.cluster.dto.User;
import org.clevertec.cluster.exceptions.NoSuchUserException;
import org.clevertec.cluster.repository.UserRepository;
import org.clevertec.cluster.service.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = "cluster.node-number = 1")
@SpringBootTest
public class UserServiceImplTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private CacheOperations cacheOperations;

    @Autowired
    private UserServiceImpl userService;

    private final User testUser = new User(1, "nickname1", "country1",
            "city1", "login1", "password1");

    private final Optional<User> optionalTestUser = Optional.of(testUser);

    private final List<User> users = Arrays.asList(
            new User(1, "nickname1", "country1", "city1", "login1", "password1"),
            new User(2, "nickname2", "country2", "city2", "login2", "password2"),
            new User(3, "nickname3", "country3", "city3", "login3", "password3")
    );


    @Test
    public void saveUser(){
        given(this.userRepository.save(testUser)).willReturn(testUser);

        userService.save(testUser);
        Mockito.verify(userRepository).save(testUser);
    }

    @Test
    public void findUser() {
        given(this.cacheOperations.getById(1)).willReturn(null);
        given(this.userRepository.findById(1)).willReturn(optionalTestUser);

        doNothing().when(cacheOperations).add(optionalTestUser.orElse(new User()));

        Optional<User> user = userService.find(1);
        assertEquals(optionalTestUser, user);
    }

    @Test(expected = NoSuchUserException.class)
    public void findNonexistentUser(){
        given(this.cacheOperations.getById(1)).willReturn(null);
        given(this.userRepository.findById(1)).willReturn(Optional.empty());

        userService.find(1);
    }

    @Test
    public void updateUser(){
        doNothing().when(cacheOperations).update(testUser);

        given(this.userRepository.save(testUser)).willReturn(testUser);
        given(this.userRepository.findById(testUser.getId())).willReturn(optionalTestUser);

        userService.update(testUser);
        Mockito.verify(cacheOperations).update(testUser);
    }


    @Test(expected = NoSuchUserException.class)
    public void updateNonexistentUser(){
        given(this.userRepository.findById(testUser.getId())).willReturn(Optional.empty());

        userService.update(testUser);
    }

    @Test
    public void deleteUser(){
        doNothing().when(userRepository).deleteById(1);
        doNothing().when(cacheOperations).delete(1);

        given(this.userRepository.findById(1)).willReturn(optionalTestUser);

        userService.delete(1);
        Mockito.verify(cacheOperations).delete(1);
    }

    @Test(expected = NoSuchUserException.class)
    public void deleteNonexistentUser(){
        given(this.userRepository.findById(testUser.getId())).willReturn(Optional.empty());

        userService.delete(testUser.getId());
    }
}