package org.clevertec.cluster.cache.logic.impl;

import org.clevertec.cluster.dto.User;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class LRUCacheLogicTest {

    private LRUCacheLogic cacheLogic = new LRUCacheLogic(100);

    private final User testUser1 = new User(1, "Nickname1", "Country1", "City1",
            "Login1", "Password1");
    private final User testUser2 = new User(2, "Nickname2", "Country2", "City2",
            "Login2", "Password2");
    private final User testUser3 = new User(3, "Nickname3", "Country3", "City3",
            "Login3", "Password3");
    private final User testUser4 = new User(4, "Nickname4", "Country4", "City4",
            "Login4", "Password4");

    @Test
    public void checkForUserExistenceInLRUCache(){
        cacheLogic.add(testUser1);
        cacheLogic.add(testUser2);
        cacheLogic.add(testUser3);
        Assert.assertTrue(cacheLogic.isExistInCache(testUser1.getId()));
        Assert.assertFalse(cacheLogic.isExistInCache(testUser4.getId()));
    }

    @Test
    public void addUserInLRUCache() {
        cacheLogic.add(testUser1);
        cacheLogic.add(testUser2);
        cacheLogic.add(testUser3);
        cacheLogic.add(testUser4);
        Assert.assertEquals(4, cacheLogic.getCache().size());
    }

    @Test
    public void updateUserInLRUCache(){
        User updatedUser = new User(3, "Nickname31", "Country31", "City31", "Login31", "Password31");
        cacheLogic.add(testUser1);
        cacheLogic.add(testUser2);
        cacheLogic.add(testUser3);
        cacheLogic.add(testUser4);
        cacheLogic.update(updatedUser);
        Assert.assertEquals(updatedUser, cacheLogic.getById(3));
    }

    @Test
    public void updateNonexistentUserInLRUCache(){
        cacheLogic.update(testUser1);
        Assert.assertEquals(testUser1, cacheLogic.getById(1));
    }

    @Test
    public void findUserByIdFromLRUCache(){
        cacheLogic.add(testUser1);
        cacheLogic.add(testUser2);
        cacheLogic.add(testUser3);
        cacheLogic.add(testUser4);
        Assert.assertEquals(cacheLogic.getById(3), testUser3);
    }

    @Test
    public void findNotExistUserByIdFromLRUCache(){
        cacheLogic.add(testUser1);
        cacheLogic.add(testUser2);
        cacheLogic.add(testUser3);
        cacheLogic.add(testUser4);
        Assert.assertNull(cacheLogic.getById(6));
    }

    @Test
    public void deleteUserFromLRUCache(){
        cacheLogic.add(testUser1);
        cacheLogic.add(testUser2);
        cacheLogic.add(testUser3);
        cacheLogic.add(testUser4);
        cacheLogic.deleteById(3);
        Assert.assertNull(cacheLogic.getById(3));
    }

    @Test
    public void deleteNotActualUserFromLRUCache(){
        User newUser = new User(500, "Nickname500", "Country500", "City500", "Login500", "Password500");
        for(int i = 0; i < cacheLogic.getCacheCapacity(); i++){
            User user = new User(i + 1, "Nickname " + i, "Country"+ i, "City"+ i, "Login"+ i,
                    "Password"+ i);
            cacheLogic.add(user);
        }
        for(int i = 0; i < cacheLogic.getCacheCapacity(); i++){
            User updatedUser = new User(i + 1, "Nickname " + i + 1, "Country"+ i + 1,
                    "City"+ i + 1, "Login"+ i + 1, "Password"+ i + 1);
            if(i == 56){
                continue;
            }
            cacheLogic.update(updatedUser);
        }
        cacheLogic.add(newUser);
        Assert.assertThat(cacheLogic.getCache().size(), is(100));
        Assert.assertEquals(cacheLogic.getById(500), newUser);
        Assert.assertNull(cacheLogic.getById(57));
    }
}