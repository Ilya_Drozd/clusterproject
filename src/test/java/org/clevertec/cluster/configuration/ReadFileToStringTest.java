package org.clevertec.cluster.configuration;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReadFileToStringTest {

    private final static String  CONF_FILE_NAME = "ClusterConfiguration.txt";

    private final ReadFileToString readFile = new ReadFileToString();

    @Test
    public void readFile() {
        String line = "node1 {port: 9000, schema: schema1};" +
                "node2 {port: 9001, schema: schema2};" +
                "node3 {port: 9002, schema: schema3}";

        assertEquals(line, readFile.readFile(CONF_FILE_NAME));
    }
}