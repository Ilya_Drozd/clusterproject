package org.clevertec.cluster.configuration;

import org.junit.Test;

import java.util.LinkedHashMap;

import static org.junit.Assert.*;

public class ConfFileParserTest {

    private final String line = "node1 {port: 9000, schema: schema1};\n" +
                                "node2 {port: 9001, schema: schema2};\n" +
                                "node3 {port: 9002, schema: schema3}";

    private LinkedHashMap<Integer, Integer> ports = new LinkedHashMap<>();
    private LinkedHashMap<Integer, String> schemas = new LinkedHashMap<>();

    private final ConfFileParser parser = new ConfFileParser();

    @Test
    public void parseLinePortsTest(){
        ports.put(1, 9000);
        ports.put(2, 9001);
        ports.put(3, 9002);

        parser.parseLine(line);

        assertEquals(ports, parser.getPorts());
    }

    @Test
    public void parseLineSchemaTest(){
        schemas.put(1, "schema1");
        schemas.put(2, "schema2");
        schemas.put(3, "schema3");

        parser.parseLine(line);

        assertEquals(schemas, parser.getSchemas());
    }

}