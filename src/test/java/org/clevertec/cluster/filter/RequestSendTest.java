package org.clevertec.cluster.filter;

import org.clevertec.cluster.dto.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"cluster.node-number = 1"})
@SpringBootTest
public class RequestSendTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private RequestSend requestSend;

    private final User user = new User(1, "Nickname1", "Country1", "City1", "Login1",
            "Password1");
    private final HttpEntity<User> requestBody = new HttpEntity<>(user);


    @Test
    public void sendPostRequest() throws Exception {
        URI uri = new URI("");

        given(this.restTemplate.postForLocation("", requestBody, User.class)).willReturn(uri);

        requestSend.sendRequest("POST", user, "");
        Mockito.verify(restTemplate).postForLocation("", requestBody, User.class);
    }

    @Test
    public void sendPutRequest() throws Exception {
        doNothing().when(restTemplate).put("", requestBody, User.class);

        requestSend.sendRequest("PUT", user, "");
        Mockito.verify(restTemplate).put("", requestBody, User.class);
    }

    @Test
    public void sendDeleteRequest() throws Exception {
        doNothing().when(restTemplate).delete("");

        requestSend.sendRequest("DELETE", user, "");
        Mockito.verify(restTemplate).delete("");
    }
}