package org.clevertec.cluster.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.clevertec.cluster.configs.SpringJdbcConfig;
import org.clevertec.cluster.dto.User;
import org.clevertec.cluster.service.UserService;
import org.clevertec.cluster.status.ClusterStatusClarification;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"cluster.node-number = 2", "server.port = 9001", "ports.port1 = 9000",
        "ports.port2 = 9001", "ports.port3 = 9002"})
@WebMvcTest
@AutoConfigureWebClient
@ComponentScan(value = {"org.clevertec.cluster.configs", "org.clevertec.cluster.status", "org.clevertec.cluster.filter"},
        excludeFilters={@ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE, value= SpringJdbcConfig.class)})
public class AppFilterTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private UserService userService;
    @MockBean
    private RequestSend requestSend;

    private final User user1 = new User(1, "nickname1", "country1", "city1",
            "login1", "password1");
    private final User user2 = new User(2, "nickname2", "country2", "city2",
            "login2", "password2");
    private final List<User> users = Arrays.asList(
            new User(1, "nickname1", "country1", "city1", "login1", "password1"),
            new User(2, "nickname2", "country2", "city2", "login2", "password2"),
            new User(3, "nickname3", "country3", "city3", "login3", "password3")
    );
    private final Page<User> page = new PageImpl<>(users);

    @BeforeClass
    public static void setNodeStatus() {
        HashMap<Integer, Boolean> nodes = new HashMap<>();
        nodes.put(9000, false);
        nodes.put(9001, true);
        nodes.put(9002, true);
        ClusterStatusClarification.setNodes(nodes);
    }

    @Test
    public void getRequestFindAllUsersPageableAndExpectStatusIsOk() throws Exception {
        given(this.userService.findAll(anyInt(), anyInt())).willReturn(page);

        this.mockMvc.perform(get("http://localhost:9000/users/all?pagesize=10&pagenumber=0")
                .param("pagenumber", "0")
                .param("pagesize", "10"))
                .andExpect(status().isOk());
    }

    @Test
    public void getRequestClusterStatusAndExpectStatusIsOk() throws Exception {
        this.mockMvc.perform(get("http://localhost:9000/status"))
                .andExpect(status().isOk());
    }

    @Test
    public void postRequestSaveUserToTheSameNodeAndExpectStatusIsOk() throws Exception {
        given(this.userService.save(user1)).willReturn(user1.getId());

        this.mockMvc.perform(post("http://localhost:9001/users/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(user1)))
                .andExpect(status().isOk());
    }

    @Test
    public void getRequestFindUserToTheSameNodeAndExpectStatusIsOk() throws Exception {
        given(this.userService.find(1)).willReturn(Optional.of(user1));

        this.mockMvc.perform(get("http://localhost:9001/users/user/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteRequestDeleteUserOnTheSameNodeAndExpectStatusIsOk() throws Exception {
        given(this.userService.delete(1)).willReturn(1);

        this.mockMvc.perform(delete("http://localhost:9001/users/user/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void postRequestSaveUserToAnotherNode() throws Exception {
        doNothing().when(requestSend).sendRequest(eq("POST"), eq(user2), eq("http://localhost:9002/users/user"));

        this.mockMvc.perform(post("http://localhost:9001/users/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(user2)));
        verify(requestSend).sendRequest(eq("POST"), eq(user2), eq("http://localhost:9002/users/user"));
    }

    @Test
    public void getRequestFindUserFromAnotherNodeAndExpectStatusIsRedirect() throws Exception {
        given(this.userService.find(2)).willReturn(Optional.of(user2));

        this.mockMvc.perform(get("http://localhost:9001/users/user/2"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void getRequestFindUserFromUnavailableNodeAndExpectStatusIsServiceUnavailable() throws Exception {
        this.mockMvc.perform(get("http://localhost:9000/users/user/3"))
                .andExpect(status().isServiceUnavailable());
    }

    @Test
    public void getRequestFindUserAndExpectStatusIsBadRequest() throws Exception {
        this.mockMvc.perform(get("http://localhost:9001/users/user/j1"))
                .andExpect(status().isBadRequest());
    }

}