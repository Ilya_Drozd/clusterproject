package org.clevertec.cluster.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.clevertec.cluster.configs.SpringJdbcConfig;
import org.clevertec.cluster.dto.User;
import org.clevertec.cluster.service.UserService;
import org.clevertec.cluster.status.ClusterStatusClarification;
import org.hamcrest.CoreMatchers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"cluster.node-number = 1", "server.port = 8082", "ports.port2 = 8082"})
@WebMvcTest
@AutoConfigureWebClient
@ComponentScan(value = {"org.clevertec.cluster.configs", "org.clevertec.cluster.status", "org.clevertec.cluster.filter"},
        excludeFilters={@ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE, value= SpringJdbcConfig.class)})
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private UserService userService;


    private final User user = new User(1, "nickname", "country", "city", "login", "password");
    private final List<User> users = Arrays.asList(
            new User(1, "nickname1", "country1", "city1", "login1", "password1"),
            new User(2, "nickname2", "country2", "city2", "login2", "password2"),
            new User(3, "nickname3", "country3", "city3", "login3", "password3")
    );
    private final Page<User> page = new PageImpl<>(users);

    @BeforeClass
    public static void setNodeStatus(){
        HashMap<Integer, Boolean> nodes = new HashMap<>();
        nodes.put(8081, true);
        nodes.put(8082, true);
        nodes.put(8083, true);
        ClusterStatusClarification.setNodes(nodes);
    }


    @Test
    public void postRequestSaveUserAndExpectStatusIsOk() throws Exception {
        given(this.userService.save(user))
                .willReturn(user.getId());

        this.mockMvc.perform(post("http://localhost:8082/users/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

    @Test
    public void getRequestFindUserAndExpectStatusIsOkAndContentJSON() throws Exception {
        given(this.userService.find(1)).willReturn(Optional.of(user));

        this.mockMvc.perform(get("http://localhost:8082/users/user/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void getRequestFindUserAndExpectJsonPathIdNickNameCountryCityLoginPassword() throws Exception {
        given(this.userService.find(1)).willReturn(Optional.of(user));

        this.mockMvc.perform(get("http://localhost:8082/users/user/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("nickname").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("nickname", CoreMatchers.equalTo("nickname")))
                .andExpect(MockMvcResultMatchers.jsonPath("country").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("country", CoreMatchers.equalTo("country")))
                .andExpect(MockMvcResultMatchers.jsonPath("city").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("city", CoreMatchers.equalTo("city")))
                .andExpect(MockMvcResultMatchers.jsonPath("login").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("login", CoreMatchers.equalTo("login")))
                .andExpect(MockMvcResultMatchers.jsonPath("password").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("password", CoreMatchers.equalTo("password")));
    }

    @Test
    public void getRequestFindAllUsersPageableAndExpectStatusIsOk() throws Exception {
        given(this.userService.findAll(anyInt(), anyInt())).willReturn(page);

        this.mockMvc.perform(get("http://localhost:8082/users/all?pagesize=10&pagenumber=0")
                .param("pagenumber", "0")
                .param("pagesize", "10"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void putRequestUpdateUserAndExpectStatusIsOk() throws Exception{
        given(this.userService.update(user)).willReturn(user.getId());

        this.mockMvc.perform(put("http://localhost:8082/users/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteRequestDeleteUserAndExpectStatusIsOk() throws Exception {
        given(this.userService.delete(1)).willReturn(1);

        this.mockMvc.perform(delete("http://localhost:8082/users/user/1"))
                .andExpect(status().isOk());
    }
}