# User service

## Description
Microservice for working with users

## REST-services:
        
#### POST [http://localhost:9000/users/user](http://localhost:9000/users/user)
#### registration of a user

        Example of request body:
        {
        	"id": "1",
        	"nickname":"nickname1",
        	"country":"country1",
        	"city":"city1",
        	"login":"login1",
        	"password":"password1"
        }

---

#### GET [http://localhost:9000/users/all?pagesize={pagesize}&pagenumber={pagenumber}](http://localhost:9000/users/all?pagesize={pagesize}&pagenumber={pagenumber})
#### receiving all users

        where:
        {pagesize} - number of elements per page (optional parameter, default 15)
        {pagenumber} - page number (optional parameter, default 1)

---

#### GET [http://localhost:9000/users/user/{userId}](http://localhost:9000/users/user/{userId})
#### receiving user by id

        where:
        {userId} - identity number

---

#### PUT [http://localhost:9000/users/user](http://localhost:9000/users/user)
#### user update

        Example of request body:
        {
            "id": "21",
        	"nickname":"nickname210",
        	"country":"country210",
        	"city":"city210",
        	"login":"login210",
        	"password":"password210"
        }

---

#### DELETE [http://localhost:9000/users/user/{userId}](http://localhost:9000/users/user/{userId})
#### user delete

        where:
        {userId} - identity number

---

  